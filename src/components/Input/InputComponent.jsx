import React, { Component } from 'react';
import PropTypes from 'prop-types';


const propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  id: PropTypes.string.isRequired,
  onChangeHandle: PropTypes.PropTypes.func,
  required: PropTypes.bool,
  minValue: PropTypes.any,
  val: PropTypes.any,
};

const defaultProps = {
  type: 'text',
  onChangeHandle: undefined,
  required: false,
  minValue: undefined,
  val: undefined,
};

export default class InputComponent extends Component {
  render() {
    const {
      label, type, id, onChangeHandle, required, minValue, val,
    } = this.props;

    return (
      <React.Fragment>
        <div className="input-group input-group-sm mb-1">
          <span className="input-group-text" id={id}>{label}</span>
          <input
            type={type}
            className="form-control"
            id={id} placeholder={label}
            onChange={onChangeHandle}
            required={required}
            min={minValue}
            value={val}
          />
        </div>
      </React.Fragment>
    );
  }
}

InputComponent.propTypes = propTypes;
InputComponent.defaultProps = defaultProps;

