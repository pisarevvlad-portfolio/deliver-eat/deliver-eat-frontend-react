import React, { Component } from 'react';
import PropTypes from 'prop-types';


const propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string,
  id: PropTypes.string.isRequired,
  onChangeHandle: PropTypes.PropTypes.func,
  val: PropTypes.any,
};

const defaultProps = {
  type: 'checkbox',
  onChangeHandle: undefined,
  val: undefined,
};

export default class CheckBoxComponent extends Component {
  render() {
    const {
      label, type, id, onChangeHandle, val,
    } = this.props;

    return (
      <React.Fragment>
        <div className="form-check mb-1">
          <input
            type={type}
            className="form-check-input"
            id={id}
            onChange={onChangeHandle}
            value={val}
          />
          <label className="form-check-label" htmlFor={id}>{label}</label>
        </div>
      </React.Fragment>
    );
  }
}

CheckBoxComponent.propTypes = propTypes;
CheckBoxComponent.defaultProps = defaultProps;

