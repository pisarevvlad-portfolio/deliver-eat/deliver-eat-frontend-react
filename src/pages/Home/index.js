import React, { Component } from 'react';
import dateFormat from "dateformat";
import {NavLink} from "react-router-dom";
import {backendURL} from '../../utils/env';

export default class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  componentDidMount() {
    fetch(`${backendURL}/api/order-eat/all`)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.response.items,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        },
      );
  }

  render() {
    const {error, isLoaded, items} = this.state;
    if (error) {
      return (
        <div>
          {`Error: ${error.message}`}
        </div>
      );
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <React.Fragment>
          <div className="table-responsive">
            <table className="table table-striped table-hover table-bordered caption-top text-center">
              <caption>Таблица со всеми заказами</caption>
              <thead className="table-dark">
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">ФИО</th>
                  <th scope="col">Телефон клиента</th>
                  <th scope="col">Дата создания заказа</th>
                  <th scope="col">Название рациона питания</th>
                  <th scope="col">Период доставки</th>
                  <th scope="col">Расписание доставки</th>
                  <th scope="col">Дни недели питания</th>
                  <th scope="col">Комментарий к заказу (если указан)</th>
                </tr>
              </thead>
              <tfoot></tfoot>
              <tbody>
                {items.map((item) => (
                  <tr key={item.id}>
                    <td><NavLink to={`/order/${item.id}`}>{item.id}</NavLink></td>
                    <td>{item.fio}</td>
                    <td>{item.phoneNumber}</td>
                    <td>{dateFormat(new Date(item.createdAt), 'yyyy-mm-dd')}</td>
                    <td>{item.dietName}</td>
                    <td>{dateFormat(new Date(item.startDelivery), 'yyyy-mm-dd')} - {dateFormat(new Date(item.endDelivery), 'yyyy-mm-dd')}</td>
                    <td>{item.periodDelivery === 1 ? 'ежедневно' : item.periodDelivery === 2 ? 'доставка через день на один день питания' : 'доставка через день на 2 дня питания'}</td>
                    <td>
                      {
                        item.daysEating.map((numberKey) => (
                          `${numberKey === '1' ? 'понедельник ' :
                            numberKey === '2' ? 'вторник ' :
                            numberKey === '3' ? 'среда ' :
                            numberKey === '4' ? 'четверг ' :
                            numberKey === '5' ? 'пятница ' :
                            numberKey === '6' ? 'суббота ' : 'воскресенье '
                          }`
                        ))
                      }
                    </td>
                    <td>{item.comments}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </React.Fragment>
      );
    }
  }
}
