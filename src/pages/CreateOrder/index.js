import React, { Component } from 'react';
import dateFormat from "dateformat";
import {backendURL} from '../../utils/env';
import {CheckBox, Input} from "../../components";

export default class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fio: null,
      phone: null,
      diet_name: null,
      period_delivery: 3,
      days_eating: [],
      comments: null,
      start_delivery: new Date(),
      end_delivery: null,
      isLoaded: false,
      response: [],
      error: null,
      httpStatus: null,
      metaError: null
    }

    this.handleChangeFIO = this.handleChangeFIO.bind(this);
    this.handleChangePhone = this.handleChangePhone.bind(this);
    this.handleChangeDietName = this.handleChangeDietName.bind(this);
    this.handleChangePeriod = this.handleChangePeriod.bind(this);
    this.handleChangeEatDay = this.handleChangeEatDay.bind(this);
    this.handleChangeComment = this.handleChangeComment.bind(this);
    this.handleChangeStartDelivery = this.handleChangeStartDelivery.bind(this);
    this.handleChangeEndDelivery = this.handleChangeEndDelivery.bind(this);
    this.handleSubmit = this.handleOnSubmitForm.bind(this);
  }

  handleOnSubmitForm(event) {
    const formData = new FormData();
    formData.append('fio', this.state.fio);
    formData.append('phone', this.state.phone);
    formData.append('diet_name', this.state.diet_name);
    formData.append('period_delivery', this.state.period_delivery);
    formData.append('days_eating', this.state.days_eating);
    formData.append('comments', this.state.comments);
    formData.append('start_delivery', dateFormat(this.state.start_delivery, 'yyyy-mm-dd'));
    formData.append('end_delivery', dateFormat(this.state.end_delivery, 'yyyy-mm-dd'));
    const requestOptions = {
      method: 'POST',
      body: formData
    };

    fetch(`${backendURL}/api/order-eat`, requestOptions)
      .then((res) => {
        if (res.status !== 200) {
          this.setState({
            httpStatus: res.status,
          });
        }
        res.json()
          .then((result) => {
              this.setState({
                isLoaded: true,
                response: result.response,
              });
              if (res.status !== 200) {
                alert('При выполнении запроса вознилка ошибка: '+result.meta.error)
              } else {
                window.open('/order/'+result.response.id, '_self');
              }
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error,
              });
            });
      });
  }

  handleChangeFIO(event) {
    this.setState({
      fio: event.target.value
    });
  }

  handleChangePhone(event) {
    this.setState({
      phone: event.target.value
    });
  }
  handleChangeDietName(event) {
    this.setState({
      diet_name: event.target.value
    });
  }
  handleChangePeriod(event) {
    this.setState({
      period_delivery: event.target.value
    });
  }
  handleChangeEatDay(event) {
    const numberDay = event.target.value;
    if (this.state.days_eating.includes(numberDay)) {
      if (!event.target.checked) {
        this.state.days_eating.splice(this.state.days_eating.indexOf(numberDay), 1)
      }
    } else {
      this.state.days_eating.push(numberDay);
    }
  }
  handleChangeComment(event) {
    this.setState({
      comments: event.target.value
    });
  }

  handleChangeStartDelivery(event) {
    const date = new Date(event.target.value);
    if (date.setHours(0,0,0,0) >= new Date().setHours(0,0,0,0)) {
      this.setState({
        start_delivery: date
      });
    }
  }
  handleChangeEndDelivery(event) {
    const date = new Date(event.target.value);
    if (new Date().setHours(0,0,0,0) <= date.setHours(0,0,0,0)) {
      this.setState({
        end_delivery: date
      });
    } else {
      event.target.value = this.state.end_delivery;
      alert('Дата окончания заказа не может быть меньше даты начала');
    }
  }

  render() {
    return (
      <React.Fragment>
        <form>
          <Input label={"ФИО Заказчика"} id={"fio"} onChangeHandle={this.handleChangeFIO} required={true}/>
          <Input label={"Номер телефона заказчика"} id={"phone"} onChangeHandle={this.handleChangePhone} required={true}/>
          <Input label={"Название рациона питания"} id={"product"} onChangeHandle={this.handleChangeDietName} required={true}/>

          <Input label={"Дата начала выполнения заказа"} id={"startDate"} onChangeHandle={this.handleChangeStartDelivery} required={true} minValue={dateFormat(new Date(), 'yyyy-mm-dd')} type={"date"}/>
          <Input label={"Дата окончания выполнения заказа"} id={"startDate"} onChangeHandle={this.handleChangeEndDelivery} required={true} minValue={dateFormat(new Date(), 'yyyy-mm-dd')} type={"date"}/>

          <div className="input-group input-group-sm mb-1">
            <span className="input-group-text">Период доставки</span>
            <select className={"form-select"} multiple={false} required={true} onChange={this.handleChangePeriod} defaultValue={this.state.period_delivery}>
              <option value={1}>Ежедневно</option>
              <option value={2}>Доставка через день на один день питания</option>
              <option value={3}>Доставка через день на 2 дня питания</option>
            </select>
          </div>

          <p>Выберите даты питания:</p>
          <CheckBox label={"Понедельник"} id={"monday"} onChangeHandle={this.handleChangeEatDay} val={1}/>
          <CheckBox label={"Вторник"} id={"tuesday"} onChangeHandle={this.handleChangeEatDay} val={2}/>
          <CheckBox label={"Среда"} id={"wednesday"} onChangeHandle={this.handleChangeEatDay} val={3}/>
          <CheckBox label={"Четверг"} id={"thursday"} onChangeHandle={this.handleChangeEatDay} val={4}/>
          <CheckBox label={"Пятница"} id={"friday"} onChangeHandle={this.handleChangeEatDay} val={5}/>
          <CheckBox label={"Суббота"} id={"saturday"} onChangeHandle={this.handleChangeEatDay} val={6}/>
          <CheckBox label={"Воскресенье"} id={"sunday"} onChangeHandle={this.handleChangeEatDay} val={7}/>

          <div className="mb-3">
            <label htmlFor="comment" className="form-label">Комментарий к заказу</label>
            <textarea className="form-control" id="comment" rows="3" required={false} onChange={this.handleChangeComment} />
          </div>
          <button className={"btn btn-outline-primary"} type={"button"} onClick={this.handleSubmit}>Создать заказ</button>
        </form>
      </React.Fragment>
    );
  }
}
