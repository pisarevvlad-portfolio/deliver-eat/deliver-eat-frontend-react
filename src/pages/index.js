export {
  default as HomePage,
} from './Home';

export {
  default as OrderPage,
} from './Order';

export {
  default as CreateOrder,
} from './CreateOrder'
