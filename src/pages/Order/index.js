import React, { Component } from 'react';
import {backendURL} from "../../utils/env";
import {withRouter} from "react-router-dom";
import dateFormat from "dateformat";

export class OrderPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: undefined,
      isLoaded: false,
      item: {},
      httpStatus: 200,
    };
  }

  componentDidMount() {
    fetch(`${backendURL}/api/order-eat/${this.props.match.params.order}/estimated`)
      .then((res) => {
        if (res.status !== 200) {
          this.setState({
            httpStatus: res.status,
          });
        }
        res.json()
          .then((result) => {
              this.setState({
                isLoaded: true,
                item: result.response,
              });
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error,
              });
            });
      });
  }

  render() {
    const {
      error,
      isLoaded,
      item,
      httpStatus,
    } = this.state;
    if (error || httpStatus !== 200) {
      if (httpStatus !== 200) {
        if (httpStatus === 404) {
          return (
            <React.Fragment>
              <div>
                {`Error: ${error}`}
                {`Error - status: ${httpStatus}`}
              </div>
            </React.Fragment>
          );
        }
        return (
          <React.Fragment>
            <div>
              {`Error: ${error}`}
              {`Error - status: ${httpStatus}`}
            </div>
          </React.Fragment>
        );
      }
      return (
        <div>
          {`Error: ${error}`}
          {`Error - status: ${httpStatus}`}
        </div>
      );
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <React.Fragment>
          <p><b>Заказ №</b>{item.order.id}</p>
          <p><b>ФИО заказчика - </b>{item.order.fio}</p>
          <p><b>Номер телефона заказчика - </b>{item.order.phoneNumber}</p>
          <p><b>Дата формирования заказа - </b>{dateFormat(new Date(item.order.createdAt), 'yyyy-mm-dd')}</p>
          <p><b>Название рациона - </b>{item.order.dietName}</p>
          <p><b>С какого по какое доставлять - </b>{dateFormat(new Date(item.order.startDelivery), 'yyyy-mm-dd')} - {dateFormat(new Date(item.order.endDelivery), 'yyyy-mm-dd')}</p>
          <p><b>Периодичность доставки - </b>{item.order.periodDelivery === 1 ? 'ежедневно' : item.order.periodDelivery === 2 ? 'доставка через день на один день питания' : 'доставка через день на 2 дня питания'}</p>
          <p><b>По каким дням питается - </b>
            {
              item.order.daysEating.map((numberKey) => (
                `${numberKey === '1' ? 'понедельник ' :
                  numberKey === '2' ? 'вторник ' :
                    numberKey === '3' ? 'среда ' :
                      numberKey === '4' ? 'четверг ' :
                        numberKey === '5' ? 'пятница ' :
                          numberKey === '6' ? 'суббота ' : 'воскресенье '
                }`
              ))
            }
          </p>
          <p><b>Комментарий к заказу - </b>{item.order.comments}</p>
          <table className={"table table-striped table-hover table-bordered caption-top text-center"}>
            <caption>Расписание доставки</caption>
            <thead>
            <tr>
              <th>Дата доставки</th>
              <th>Кол-во порций</th>
            </tr>
            </thead>
            <tfoot></tfoot>
            <tbody>
            {item.delivery.map((delivery) => (
              <tr key={delivery.index}>
                <td>{delivery.delivery_date}</td>
                <td>{delivery.delivery_count}</td>
              </tr>
            ))}
            </tbody>
          </table>
        </React.Fragment>
      );
    }
  }
}

export default withRouter(OrderPage);

