import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter, Switch, Route,
} from 'react-router-dom';
import {
  CreateOrder,
  HomePage, OrderPage,
} from './pages';
import reportWebVitals from './reportWebVitals';
import {AppLayout} from "./layouts";

ReactDOM.render(
  <BrowserRouter>
    <AppLayout>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/order/:order" component={OrderPage} />
        <Route path="/add" component={CreateOrder} />
      </Switch>
    </AppLayout>
  </BrowserRouter>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
