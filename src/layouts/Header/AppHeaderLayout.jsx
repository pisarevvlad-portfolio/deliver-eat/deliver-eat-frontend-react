import React from 'react';
import {NavLink} from "react-router-dom";

function AppHeaderLayout() {
  return (
    <React.Fragment>
      <NavLink to="/">Главная</NavLink>
      <NavLink to="/add">Добавить заказ</NavLink>
    </React.Fragment>
  );
}

export default AppHeaderLayout;
