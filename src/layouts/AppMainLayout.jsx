import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { AppHeaderLayout } from './Header';

const propTypes = {
  children: PropTypes.node.isRequired,
};

export default class AppMainLayout extends Component {
  render() {
    const {
      children,
    } = this.props;

    return (
      <React.Fragment>
        <AppHeaderLayout />
        {children}
      </React.Fragment>
    );
  }
}

AppMainLayout.propTypes = propTypes;
