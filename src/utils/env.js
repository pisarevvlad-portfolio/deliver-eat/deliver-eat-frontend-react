/**
 * URL address of backend - API URL
 */
export const backendURL = process.env.REACT_APP_BACKEND_URL;
